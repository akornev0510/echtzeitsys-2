#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

int total_wait_time = 0;  // Globale Variable zur Speicherung der gesamten Wartezeit

void *thread_function(void *arg) {
    int thread_id = *(int*)arg;
    int wait_time = rand() % 21;  // Generiere eine Zufallszahl zwischen 0 und 20

    printf("Thread %d gestartet\n", thread_id);
    sleep(wait_time);  // Warte für wait_time Sekunden
    printf("Ende des Threads, der bis zu %d Sekunden gezählt hat\n", wait_time);

    total_wait_time += wait_time;  // Inkrementiere die gesamte Wartezeit

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Verwendung: %s <Anzahl_der_Threads>\n", argv[0]);
        return 1;
    }

    int n = atoi(argv[1]);
    if (n <= 0) {
        printf("Die Anzahl der Threads muss eine positive Ganzzahl sein.\n");
        return 1;
    }

    pthread_t *threads = malloc(n * sizeof(pthread_t));  // Array für Thread-IDs
    int *thread_ids = malloc(n * sizeof(int));  // Array für Thread-IDs

    srand(time(NULL));  // Initialisiere den Zufallszahlengenerator mit der aktuellen Zeit

    for (int i = 0; i < n; i++) {
        thread_ids[i] = i + 1;

        int res = pthread_create(&threads[i], NULL, thread_function, &thread_ids[i]);
        if (res != 0) {
            perror("Fehler beim Erstellen des Threads");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < n; i++) {
        int join_res = pthread_join(threads[i], NULL);
        if (join_res != 0) {
            perror("Fehler beim Warten auf den Thread");
            exit(EXIT_FAILURE);
        }
    }

    printf("Gesamte Wartezeit der Threads: %d Sekunden\n", total_wait_time);

    free(threads);
    free(thread_ids);

    return 0;
}
